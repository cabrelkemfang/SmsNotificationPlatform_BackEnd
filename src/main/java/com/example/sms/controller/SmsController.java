package com.example.sms.controller;

import com.example.sms.Domain.User;
import com.example.sms.Service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class SmsController {

@Autowired
private SmsService smsService;

@RequestMapping(value = "/sendsms",method =RequestMethod.POST)
  public String send(){
return smsService.sendSms();
}

}
