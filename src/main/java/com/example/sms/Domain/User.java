package com.example.sms.Domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "student")
public class User {

  @Id
  @Column(name = "matricule")
  private String matriculeNumber;

  @Column(name = "status")
  private String Status;

  @Column(name ="first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "data")
  private Date dateOfBirth;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  private String password;

  @Column(name = "phone")
  private Integer phoneNumber;

  @Column(name = "isActivate")
  private Boolean isActivate;

  public User() {
  }

  public User(String matriculeNumber, String status, String firstName, String lastName, Date dateOfBirth, String email, String password, Integer phoneNumber, Boolean isActivate) {
    this.matriculeNumber = matriculeNumber;
    Status = status;
    this.firstName = firstName;
    this.lastName = lastName;
    this.dateOfBirth = dateOfBirth;
    this.email = email;
    this.password = password;
    this.phoneNumber = phoneNumber;
    this.isActivate = isActivate;
  }

  public String getMatriculeNumber() {
    return matriculeNumber;
  }

  public void setMatriculeNumber(String matriculeNumber) {
    this.matriculeNumber = matriculeNumber;
  }

  public String getStatus() {
    return Status;
  }

  public void setStatus(String status) {
    Status = status;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(Integer phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Boolean getActivate() {
    return isActivate;
  }

  public void setActivate(Boolean activate) {
    isActivate = activate;
  }
}
