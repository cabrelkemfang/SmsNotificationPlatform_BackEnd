package com.example.sms.Repository;

import com.example.sms.Domain.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository  extends JpaRepository<User,String> {
}
